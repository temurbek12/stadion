FROM node:9-slim

WORKDIR /user/src/stadionNode

COPY package.json /

RUN npm install

COPY . .

EXPOSE 8080

CMD ["npm", "start"]